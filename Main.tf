

provider "aws" {
    access_key = var.aws_access_key
    secret_key = var.aws_secret_key   
    region = var.aws_region  
}





 # S3 Buckets

module "module_create_cutitronics_s3_report_store" {
    source = "./Modules/S3"
    v_environment = "${var.v_client}-${var.v_environment}"
    v_s3_bucket_name = "report-store-071220"
}


module "module_create_cutitronics_s3_lambda_layers" {
    source = "./Modules/S3"
    v_environment = "${var.v_client}-${var.v_environment}"
    v_s3_bucket_name = "lambda-layers-071220"
}


module "module_create_cutitronics_s3_lambda_source" {
    source = "./Modules/S3"
    v_environment = "${var.v_client}-${var.v_environment}"
    v_s3_bucket_name = "lambda-source-071220"
}


# RDS


module "module_create_cutitronics_rds" {
    source = "./Modules/RDS"
    v_environment = "${var.v_client}_${var.v_environment}"
    v_client_name = var.v_client
    v_rds_allocated_storage = var.v_rds_allocated_storage
    v_rds_storage_type = var.v_rds_storage_type
    v_rds_engine = var.v_rds_engine
    v_rds_engine_version = var.v_rds_engine_version
    v_rds_instance_class = var.v_rds_instance_class
    v_rds_name = var.v_rds_name
    v_rds_identifier = var.v_rds_identifier
    v_rds_username = var.v_rds_username
    v_rds_password = var.v_rds_password
    v_rds_parameter_group_name = var.v_rds_parameter_group_name
}



# Cognito


# Client Cognito
module "module_create_cognito_client" {
    source            = "./Modules/Cognito"
    name     = "${var.v_client}-${var.v_environment}-client-pool"
    username_attributes = [ "email"]
    allow_admin_create_user_only = false
    email_subject = "Your verification code"
    email_message = "Your username is {username} and your temporary password is '{####}'."
    sms_message = "{username} verification code is {####}."
    auto_verified_attributes = ["email"]
    challenge_required_on_new_device = true
    device_only_remembered_on_user_prompt = false
    enable_username_case_sensitivity = true
    minimum_length    = 10
    require_lowercase = true
    require_numbers   = true
    require_uppercase = true
    require_symbols   = true
    temporary_password_validity_days = 1
    allowed_oauth_flows = ["implicit"]
    allowed_oauth_flows_user_pool_client = true
    generate_secret  = false
    explicit_auth_flows = ["ALLOW_ADMIN_USER_PASSWORD_AUTH", "ALLOW_REFRESH_TOKEN_AUTH", "ALLOW_USER_PASSWORD_AUTH"]
    prevent_user_existence_errors = "ENABLED"
    refresh_token_validity = 30
}


# Therapist Cognito
module "module_create_cognito_therapist" {
    source            = "./Modules/Cognito"
    name     = "${var.v_client}-${var.v_environment}-therapist-pool"
    username_attributes = [ "email"]
    allow_admin_create_user_only = false
    email_subject = "Your verification code"
    email_message = "Your username is {username} and your temporary password is '{####}'."
    sms_message = "{username} verification code is {####}."
    auto_verified_attributes = ["email"]
    challenge_required_on_new_device = true
    device_only_remembered_on_user_prompt = false
    enable_username_case_sensitivity = true
    minimum_length    = 10
    require_lowercase = true
    require_numbers   = true
    require_uppercase = true
    require_symbols   = true
    temporary_password_validity_days = 1
    allowed_oauth_flows = ["implicit"]
    allowed_oauth_flows_user_pool_client = true
    generate_secret  = false
    explicit_auth_flows = ["ALLOW_ADMIN_USER_PASSWORD_AUTH", "ALLOW_REFRESH_TOKEN_AUTH", "ALLOW_USER_PASSWORD_AUTH"]
    prevent_user_existence_errors = "ENABLED"
    refresh_token_validity = 30

}


# System Cognito
module "module_create_cognito_system" {
    source            = "./Modules/Cognito"
    name     = "${var.v_client}-${var.v_environment}-system-pool"
    username_attributes = [ "email"]
    allow_admin_create_user_only = false
    email_subject = "Your verification code"
    email_message = "Your username is {username} and your temporary password is '{####}'."
    sms_message = "{username} verification code is {####}."
    auto_verified_attributes = ["email"]
    challenge_required_on_new_device = true
    device_only_remembered_on_user_prompt = false
    enable_username_case_sensitivity = true
    minimum_length    = 10
    require_lowercase = true
    require_numbers   = true
    require_uppercase = true
    require_symbols   = true
    temporary_password_validity_days = 1
    allowed_oauth_flows = ["implicit"]
    allowed_oauth_flows_user_pool_client = true
    generate_secret  = false
    explicit_auth_flows = ["ALLOW_ADMIN_USER_PASSWORD_AUTH", "ALLOW_REFRESH_TOKEN_AUTH", "ALLOW_USER_PASSWORD_AUTH"]
    prevent_user_existence_errors = "ENABLED"
    refresh_token_validity = 30
}


# Admin Cognito
module "module_create_cognito_admin" {
    source            = "./Modules/Cognito"
    name     = "${var.v_client}-${var.v_environment}-admin-pool"
    username_attributes = [ "email"]
    allow_admin_create_user_only = false
    email_subject = "Your verification code"
    email_message = "Your username is {username} and your temporary password is '{####}'."
    sms_message = "{username} verification code is {####}."
    auto_verified_attributes = ["email"]
    challenge_required_on_new_device = true
    device_only_remembered_on_user_prompt = false
    enable_username_case_sensitivity = true
    minimum_length    = 10
    require_lowercase = true
    require_numbers   = true
    require_uppercase = true
    require_symbols   = true
    temporary_password_validity_days = 1
    allowed_oauth_flows = ["implicit"]
    allowed_oauth_flows_user_pool_client = true
    generate_secret  = false
    explicit_auth_flows = ["ALLOW_ADMIN_USER_PASSWORD_AUTH", "ALLOW_REFRESH_TOKEN_AUTH", "ALLOW_USER_PASSWORD_AUTH"]
    prevent_user_existence_errors = "ENABLED"
    refresh_token_validity = 30
}


# Lambda Layers

module "module_lambda_rds_layer" {
    source            = "./Modules/Lambda/Layer"
    lv_filename = "./Source_Files/Lambda_Layer/pymysql.zip"
    lv_layer_name = "${var.v_client}_${var.v_environment}-pymysql"
}


# Lambdas


# Client-Lambdas


# createClient


module "module_lambda_Client_createClient"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Client/createClient/createClient.zip"
    lv_function_name = "Client-createClient"
    lv_handler = "createClient.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# deleteClient


module "module_lambda_Client_deleteClient"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Client/deleteClient/deleteClient.zip"
    lv_function_name = "Client-deleteClient"
    lv_handler = "deleteClient.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# getBrandProducts


module "module_lambda_Client_getBrandProducts"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Client/getBrandProducts/getBrandProducts.zip"
    lv_function_name = "Client-getBrandProducts"
    lv_handler = "getBrandProducts.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# getCartInstall




module "module_lambda_Client_getCartInstall"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Client/getCartInstall/getCartInstall.zip"
    lv_function_name = "Client-getCartInstall"
    lv_handler = "getCartInstall.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# getClient


module "module_lambda_Client_getClient"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Client/getClient/getClient.zip"
    lv_function_name = "Client-getClient"
    lv_handler = "getClient.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# getClientPrescriptions


module "module_lambda_Client_getClientPrescriptions"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Client/getClientPrescriptions/getClientPrescriptions.zip"
    lv_function_name = "Client-getClientPrescriptions"
    lv_handler = "getClientPrescriptions.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# getDefaultPrescriptions


module "module_lambda_Client_getDefaultPrescriptions"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Client/getDefaultPrescriptions/getDefaultPrescriptions.zip"
    lv_function_name = "Client-getDefaultPrescriptions"
    lv_handler = "getDefaultPrescriptions.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# getQuestions


module "module_lambda_Client_getQuestions"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Client/getQuestions/getQuestions.zip"
    lv_function_name = "Client-getQuestions"
    lv_handler = "getQuestions.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# getUsage


module "module_lambda_Client_getUsage"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Client/getUsage/getUsage.zip"
    lv_function_name = "Client-getUsage"
    lv_handler = "getUsage.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# logCartInstall


module "module_lambda_Client_logCartInstall"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Client/logCartInstall/logCartInstall.zip"
    lv_function_name = "Client-logCartInstall"
    lv_handler = "logCartInstall.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# postAnswers


module "module_lambda_Client_postAnswers"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Client/postAnswers/postAnswers.zip"
    lv_function_name = "Client-postAnswers"
    lv_handler = "postAnswers.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# reqFirmwareUpdate


module "module_lambda_ClientreqFirmwareUpdate"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Client/reqFirmwareUpdate/reqFirmwareUpdate.zip"
    lv_function_name = "Client-reqFirmwareUpdate"
    lv_handler = "reqFirmwareUpdate.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# uploadUsage


module "module_lambda_Client_uploadUsage"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Client/uploadUsage/uploadUsage.zip"
    lv_function_name = "Client-uploadUsage"
    lv_handler = "uploadUsage.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# Therapist-Lambdas


# createSession


module "module_lambda_Therapist_createSession"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Therapist/createSession/createSession.zip"
    lv_function_name = "Therapist-createSession"
    lv_handler = "createSession.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# CutitronicsLookUp


module "module_lambda_Therapist_CutitronicsLookUp"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Therapist/CutitronicsLookUp/CutitronicsLookUp.zip"
    lv_function_name = "Therapist-CutitronicsLookUp"
    lv_handler = "CutitronicsLookUp.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# getAllClients


module "module_lambda_Therapist_getAllClients"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Therapist/getAllClients/getAllClients.zip"
    lv_function_name = "Therapist-getAllClients"
    lv_handler = "getAllClients.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# getAllTherapists


module "module_lambda_Therapist_getAllTherapists"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Therapist/getAllTherapists/getAllTherapists.zip"
    lv_function_name = "Therapist-getAllTherapists"
    lv_handler = "getAllTherapists.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# getBrandProducts


module "module_lambda_Therapist_getBrandProducts"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Therapist/getBrandProducts/getBrandProducts.zip"
    lv_function_name = "Therapist-getBrandProducts"
    lv_handler = "getBrandProducts.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# getClientPrescriptions


module "module_lambda_Therapist_getClientPrescriptions"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Therapist/getClientPrescriptions/getClientPrescriptions.zip"
    lv_function_name = "Therapist-getClientPrescriptions"
    lv_handler = "getClientPrescriptions.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# getDefaultPrescriptions


module "module_lambda_Therapist_getDefaultPrescriptions"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Therapist/getDefaultPrescriptions/getDefaultPrescriptions.zip"
    lv_function_name = "Therapist-getDefaultPrescriptions"
    lv_handler = "getDefaultPrescriptions.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# getGuest


module "module_lambda_Therapist_getGuest"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Therapist/getGuest/getGuest.zip"
    lv_function_name = "Therapist-getGuest"
    lv_handler = "getGuest.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# getSession


module "module_lambda_Therapist_getSession"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Therapist/getSession/getSession.zip"
    lv_function_name = "Therapist-getSession"
    lv_handler = "getSession.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}
# getUsage


module "module_lambda_Therapist_getUsage"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Therapist/getUsage/getUsage.zip"
    lv_function_name = "Therapist-getUsage"
    lv_handler = "getUsage.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# postGuestNotes


module "module_lambda_Therapist_postGuestNotes"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Therapist/postGuestNotes/postGuestNotes.zip"
    lv_function_name = "Therapist-postGuestNotes"
    lv_handler = "postGuestNotes.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# updateSession


module "module_lambda_Therapist_updateSession"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Therapist/updateSession/updateSession.zip"
    lv_function_name = "Therapist-updateSession"
    lv_handler = "updateSession.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# getAllGuests


module "module_lambda_Therapist_getAllGuests"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Therapist/getAllGuests/getAllGuests.zip"
    lv_function_name = "Therapist-getAllGuests"
    lv_handler = "getAllGuests.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}





# Api gateway


# Gateway

module "module_lambda_Cutitronics_Gateway"{
    source            = "./Modules/APIGateway/Gateway"
    lv_name = "Main_gateway"
}


module "module_lambda_Cutitronics_legacy_Gateway"{
    source            = "./Modules/APIGateway/Gateway"
    lv_name = "legacy_gateway"
}


# Authorizers


# Client Authorizer


module "module_cognito_client_authorizer"{
    source            = "./Modules/APIGateway/Authorizers"
    lv_name = "ishgaClientAuthorizer"
    lv_provider = module.module_create_cognito_client.arn
    lv_rest_api = module.module_lambda_Cutitronics_Gateway.id     
}


# Therapist Authorizer


module "module_cognito_therapist_authorizer"{   
    source            = "./Modules/APIGateway/Authorizers"
    lv_name = "ishgaTherapistAuthorizer"
    lv_provider = module.module_create_cognito_therapist.arn
    lv_rest_api = module.module_lambda_Cutitronics_Gateway.id     
}


# System Authorizer


module "module_cognito_system_authorizer"{
    source            = "./Modules/APIGateway/Authorizers"
    lv_name = "ishgaSystemAuthorizer"
    lv_provider = module.module_create_cognito_system.arn
    lv_rest_api = module.module_lambda_Cutitronics_Gateway.id     
}


# Admin Authorizer


module "module_cognito_admin_authorizer"{
    source            = "./Modules/APIGateway/Authorizers"
    lv_name = "ishgaAdminAuthorizer"
    lv_provider = module.module_create_cognito_admin.arn
    lv_rest_api = module.module_lambda_Cutitronics_Gateway.id     
}


# Resources


module "module_api_gateway_resource_Client"{
    source            = "./Modules/APIGateway/Resources"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
    lv_parent_id = module.module_lambda_Cutitronics_Gateway.root_resource_id
    lv_part_name = "Client"
}


module "module_api_gateway_resource_Therapist"{
    source            = "./Modules/APIGateway/Resources"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
    lv_parent_id = module.module_lambda_Cutitronics_Gateway.root_resource_id
    lv_part_name = "Therapist"
}


# Create Cutitronics Validator


module "module_api_gateway_validator"{
    source            = "./Modules/APIGateway/Validator"
    lv_validator_name = "CutitronicsValidator"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
}


# Full Resources linked to Lambda


#  Client


# getBrandProducts


module "module_api_gateway_client_getBrandProducts"{
    source            = "./Modules/APIGateway/FullResource"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
    lv_validator_name = module.module_api_gateway_validator.id
    lv_authorization = "COGNITO_USER_POOLS"
    lv_authorization_scopes = ["aws.cognito.signin.user.admin"]
    lv_execution_arn = module.module_lambda_Cutitronics_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Client.id
    lv_part_name = "getBrandProducts"
    lv_method = "GET"
    lv_Authorizer = module.module_cognito_client_authorizer.id
    lv_arn = module.module_lambda_Client_getBrandProducts.arn
    lv_function_name = "Client-getBrandProducts"
    lv_Client = "Client"
    lv_input = {
        "method.request.querystring.CutitronicsBrandID"=true
    }
}


# getCartInstall


module "module_api_gateway_client_getCartInstall"{
    source            = "./Modules/APIGateway/FullResource"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
    lv_validator_name = module.module_api_gateway_validator.id
    lv_authorization = "COGNITO_USER_POOLS"
    lv_authorization_scopes = ["aws.cognito.signin.user.admin"]    
    lv_execution_arn = module.module_lambda_Cutitronics_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Client.id
    lv_part_name = "getCartInstall"
    lv_method = "GET"
    lv_Authorizer = module.module_cognito_client_authorizer.id
    lv_arn = module.module_lambda_Client_getCartInstall.arn
    lv_function_name = "Client-getCartInstall"
    lv_Client = "Client"
    lv_input = {
        "method.request.querystring.CutitronicsCartID"=true
    }
}


# getClient


module "module_api_gateway_client_getClient"{
    source            = "./Modules/APIGateway/FullResource"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
    lv_validator_name = module.module_api_gateway_validator.id
    lv_authorization = "COGNITO_USER_POOLS"
    lv_authorization_scopes = ["aws.cognito.signin.user.admin"]
    lv_execution_arn = module.module_lambda_Cutitronics_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Client.id
    lv_part_name = "getClient"
    lv_method = "GET"
    lv_Authorizer = module.module_cognito_client_authorizer.id
    lv_arn = module.module_lambda_Client_getClient.arn
    lv_function_name = "Client-getClient"
    lv_Client = "Client"
    lv_input = {
        "method.request.querystring.CutitronicsClientID"=true
    }
}


# getClientPrescriptions


module "module_api_gateway_client_getClientPrescriptions"{
    source            = "./Modules/APIGateway/FullResource"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
    lv_validator_name = module.module_api_gateway_validator.id
    lv_authorization = "COGNITO_USER_POOLS"
    lv_authorization_scopes = ["aws.cognito.signin.user.admin"]
    lv_execution_arn = module.module_lambda_Cutitronics_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Client.id
    lv_part_name = "getClientPrescriptions"
    lv_method = "GET"
    lv_Authorizer = module.module_cognito_client_authorizer.id
    lv_arn = module.module_lambda_Client_getClientPrescriptions.arn
    lv_function_name = "Client-getClientPrescriptions"
    lv_Client = "Client"
    lv_input = {
      "method.request.querystring.CutitronicsBrandID"=true
      "method.request.querystring.CutitronicsClientID"=true
    }
}


# getDefaultPrescriptions


module "module_api_gateway_client_getDefaultPrescriptions"{
    source            = "./Modules/APIGateway/FullResource"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
    lv_validator_name = module.module_api_gateway_validator.id
    lv_authorization = "COGNITO_USER_POOLS"
    lv_authorization_scopes = ["aws.cognito.signin.user.admin"]
    lv_execution_arn = module.module_lambda_Cutitronics_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Client.id
    lv_part_name = "getDefaultPrescriptions"
    lv_method = "GET"
    lv_Authorizer = module.module_cognito_client_authorizer.id
    lv_arn = module.module_lambda_Client_getDefaultPrescriptions.arn
    lv_function_name = "Client-getDefaultPrescriptions"
    lv_Client = "Client"
    lv_input = {
      "method.request.querystring.CutitronicsBrandID"=true
    }
}


# getQuestions


module "module_api_gateway_client_getQuestions"{
    source            = "./Modules/APIGateway/FullResource"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
    lv_validator_name = module.module_api_gateway_validator.id
    lv_authorization = "COGNITO_USER_POOLS"
    lv_authorization_scopes = ["aws.cognito.signin.user.admin"]
    lv_execution_arn = module.module_lambda_Cutitronics_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Client.id
    lv_part_name = "getQuestions"
    lv_method = "GET"
    lv_Authorizer = module.module_cognito_client_authorizer.id
    lv_arn = module.module_lambda_Client_getQuestions.arn
    lv_function_name = "Client-getQuestions"
    lv_Client = "Client"
    lv_input = {
      "method.request.querystring.CutitronicsBrandID"=true,
      "method.request.querystring.QuestionSet"=true
    }
}


# getUsage


module "module_api_gateway_client_getUsage"{
    source            = "./Modules/APIGateway/FullResource"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
    lv_validator_name = module.module_api_gateway_validator.id
    lv_authorization = "COGNITO_USER_POOLS"
    lv_authorization_scopes = ["aws.cognito.signin.user.admin"]
    lv_execution_arn = module.module_lambda_Cutitronics_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Client.id
    lv_part_name = "getUsage"
    lv_method = "GET"
    lv_Authorizer = module.module_cognito_client_authorizer.id
    lv_arn = module.module_lambda_Client_getUsage.arn
    lv_function_name = "Client-getUsage"
    lv_Client = "Client"
    lv_input = {
      "method.request.querystring.CutitronicsClientID"=true
      "method.request.querystring.TreatmentType"=true   
    }
}


# logCartInstall


module "module_api_gateway_client_logCartInstall"{
    source            = "./Modules/APIGateway/FullResource"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
    lv_validator_name = module.module_api_gateway_validator.id
    lv_authorization = "COGNITO_USER_POOLS"
    lv_authorization_scopes = ["aws.cognito.signin.user.admin"]    
    lv_execution_arn = module.module_lambda_Cutitronics_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Client.id
    lv_part_name = "logCartInstall"
    lv_method = "POST"
    lv_Authorizer = module.module_cognito_client_authorizer.id
    lv_arn = module.module_lambda_Client_logCartInstall.arn
    lv_function_name = "Client-logCartInstall"
    lv_Client = "Client"
}


# postAnswers


module "module_api_gateway_client_postAnswers"{
    source            = "./Modules/APIGateway/FullResource"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
    lv_validator_name = module.module_api_gateway_validator.id
    lv_authorization = "COGNITO_USER_POOLS"
    lv_authorization_scopes = ["aws.cognito.signin.user.admin"]    
    lv_execution_arn = module.module_lambda_Cutitronics_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Client.id
    lv_part_name = "postAnswers"
    lv_method = "POST"
    lv_Authorizer = module.module_cognito_client_authorizer.id
    lv_arn = module.module_lambda_Client_postAnswers.arn
    lv_function_name = "Client-postAnswers"
    lv_Client = "Client"
}


# reqFirmwareUpdate


module "module_api_gateway_client_reqFirmwareUpdate"{
    source            = "./Modules/APIGateway/FullResource"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
    lv_validator_name = module.module_api_gateway_validator.id
    lv_authorization = "COGNITO_USER_POOLS"
    lv_authorization_scopes = ["aws.cognito.signin.user.admin"]    
    lv_execution_arn = module.module_lambda_Cutitronics_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Client.id
    lv_part_name = "reqFirmwareUpdate"
    lv_method = "POST"
    lv_Authorizer = module.module_cognito_client_authorizer.id
    lv_arn = module.module_lambda_ClientreqFirmwareUpdate.arn
    lv_function_name = "Client-reqFirmwareUpdate"
    lv_Client = "Client"
}


# uploadUsage


module "module_api_gateway_client_uploadUsage"{
    source            = "./Modules/APIGateway/FullResource"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
    lv_validator_name = module.module_api_gateway_validator.id
    lv_authorization = "COGNITO_USER_POOLS"
    lv_authorization_scopes = ["aws.cognito.signin.user.admin"]    
    lv_execution_arn = module.module_lambda_Cutitronics_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Client.id
    lv_part_name = "uploadUsage"
    lv_method = "POST"
    lv_Authorizer = module.module_cognito_client_authorizer.id
    lv_arn = module.module_lambda_Client_uploadUsage.arn
    lv_function_name = "Client-uploadUsage"
    lv_Client = "Client"
}


# createClient


module "module_api_gateway_client_createClient"{
    source            = "./Modules/APIGateway/FullResource"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
    lv_validator_name = module.module_api_gateway_validator.id
    lv_authorization = "COGNITO_USER_POOLS"
    lv_authorization_scopes = ["aws.cognito.signin.user.admin"]
    lv_execution_arn = module.module_lambda_Cutitronics_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Client.id
    lv_part_name = "createClient"
    lv_method = "POST"
    lv_Authorizer = module.module_cognito_client_authorizer.id
    lv_arn = module.module_lambda_Client_createClient.arn
    lv_function_name = "Client-createClient"
    lv_Client = "Client"
}


# deleteClient


module "module_api_gateway_client_deleteClient"{
    source            = "./Modules/APIGateway/FullResource"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
    lv_validator_name = module.module_api_gateway_validator.id
    lv_authorization = "COGNITO_USER_POOLS"
    lv_authorization_scopes = ["aws.cognito.signin.user.admin"]
    lv_execution_arn = module.module_lambda_Cutitronics_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Client.id
    lv_part_name = "deleteClient"
    lv_method = "POST"
    lv_Authorizer = module.module_cognito_client_authorizer.id
    lv_arn = module.module_lambda_Client_deleteClient.arn
    lv_function_name = "Client-deleteClient"
    lv_Client = "Client"
}


#  Therapist


# createSession


module "module_api_gateway_Therapist_createSession"{
    source            = "./Modules/APIGateway/FullResource"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
    lv_validator_name = module.module_api_gateway_validator.id
    lv_authorization = "COGNITO_USER_POOLS"
    lv_authorization_scopes = ["aws.cognito.signin.user.admin"]    
    lv_execution_arn = module.module_lambda_Cutitronics_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Therapist.id
    lv_part_name = "createSession"
    lv_method = "POST"
    lv_Authorizer = module.module_cognito_therapist_authorizer.id
    lv_arn = module.module_lambda_Therapist_createSession.arn
    lv_function_name = "Therapist-createSession"
    lv_Client = "Therapist"
}


# postGuestNotes


module "module_api_gateway_Therapist_postGuestNotes"{
    source            = "./Modules/APIGateway/FullResource"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
    lv_validator_name = module.module_api_gateway_validator.id
    lv_authorization = "COGNITO_USER_POOLS"
    lv_authorization_scopes = ["aws.cognito.signin.user.admin"]    
    lv_execution_arn = module.module_lambda_Cutitronics_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Therapist.id
    lv_part_name = "postGuestNotes"
    lv_method = "POST"
    lv_Authorizer = module.module_cognito_therapist_authorizer.id
    lv_arn = module.module_lambda_Therapist_postGuestNotes.arn
    lv_function_name = "Therapist-postGuestNotes"
    lv_Client = "Therapist"
}


# updateSession


module "module_api_gateway_Therapist_updateSession"{
    source            = "./Modules/APIGateway/FullResource"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
    lv_validator_name = module.module_api_gateway_validator.id
    lv_authorization = "COGNITO_USER_POOLS"
    lv_authorization_scopes = ["aws.cognito.signin.user.admin"]    
    lv_execution_arn = module.module_lambda_Cutitronics_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Therapist.id
    lv_part_name = "updateSession"
    lv_method = "POST"
    lv_Authorizer = module.module_cognito_therapist_authorizer.id
    lv_arn = module.module_lambda_Therapist_updateSession.arn
    lv_function_name = "Therapist-updateSession"
    lv_Client = "Therapist"
}


# getAllGuests


module "module_api_gateway_Therapist_getAllGuests"{
    source            = "./Modules/APIGateway/FullResource"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
    lv_validator_name = module.module_api_gateway_validator.id
    lv_authorization = "COGNITO_USER_POOLS"
    lv_authorization_scopes = ["aws.cognito.signin.user.admin"]    
    lv_execution_arn = module.module_lambda_Cutitronics_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Therapist.id
    lv_part_name = "getAllGuests"
    lv_method = "GET"
    lv_Authorizer = module.module_cognito_therapist_authorizer.id
    lv_arn = module.module_lambda_Therapist_getAllGuests.arn
    lv_function_name = "Therapist-getAllGuests"
    lv_Client = "Therapist"
    lv_input = {
      "method.request.querystring.CutitronicsBrandID"=true
      "method.request.querystring.CutitronicsTherapistID"=true
    }
}


# CutitronicsLookUp


module "module_api_gateway_Therapist_CutitronicsLookUp"{
    source            = "./Modules/APIGateway/FullResource"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
    lv_validator_name = module.module_api_gateway_validator.id
    lv_authorization = "COGNITO_USER_POOLS"
    lv_authorization_scopes = ["aws.cognito.signin.user.admin"]    
    lv_execution_arn = module.module_lambda_Cutitronics_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Therapist.id
    lv_part_name = "CutitronicsLookUp"
    lv_method = "GET"
    lv_Authorizer = module.module_cognito_therapist_authorizer.id
    lv_arn = module.module_lambda_Therapist_CutitronicsLookUp.arn
    lv_function_name = "Therapist-CutitronicsLookUp"
    lv_Client = "Therapist"
    lv_input = {
      "method.request.querystring.CutitronicsBrandID"=true
      "method.request.querystring.CutitronicsLookUpType"=true
    }
}


# getAllClients


module "module_api_gateway_Therapist_getAllClients"{
    source            = "./Modules/APIGateway/FullResource"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
    lv_validator_name = module.module_api_gateway_validator.id
    lv_authorization = "COGNITO_USER_POOLS"
    lv_authorization_scopes = ["aws.cognito.signin.user.admin"]    
    lv_execution_arn = module.module_lambda_Cutitronics_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Therapist.id
    lv_part_name = "getAllClients"
    lv_method = "GET"
    lv_Authorizer = module.module_cognito_therapist_authorizer.id
    lv_arn = module.module_lambda_Therapist_getAllClients.arn
    lv_function_name = "Therapist-getAllClients"
    lv_Client = "Therapist"
    lv_input = {
      "method.request.querystring.CutitronicsBrandID"=true
    }
}


# getAllTherapists


module "module_api_gateway_Therapist_getAllTherapists"{
    source            = "./Modules/APIGateway/FullResource"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
    lv_validator_name = module.module_api_gateway_validator.id
    lv_authorization = "COGNITO_USER_POOLS"
    lv_authorization_scopes = ["aws.cognito.signin.user.admin"]    
    lv_execution_arn = module.module_lambda_Cutitronics_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Therapist.id
    lv_part_name = "getAllTherapists"
    lv_method = "GET"
    lv_Authorizer = module.module_cognito_therapist_authorizer.id
    lv_arn = module.module_lambda_Therapist_getAllTherapists.arn
    lv_function_name = "Therapist-getAllTherapists"
    lv_Client = "Therapist"
    lv_input = {
      "method.request.querystring.CutitronicsBrandID"=true
    }
}


# getBrandProducts


module "module_api_gateway_Therapist_getBrandProducts"{
    source            = "./Modules/APIGateway/FullResource"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
    lv_validator_name = module.module_api_gateway_validator.id
    lv_authorization = "COGNITO_USER_POOLS"
    lv_authorization_scopes = ["aws.cognito.signin.user.admin"]    
    lv_execution_arn = module.module_lambda_Cutitronics_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Therapist.id
    lv_part_name = "getBrandProducts"
    lv_method = "GET"
    lv_Authorizer = module.module_cognito_therapist_authorizer.id
    lv_arn = module.module_lambda_Therapist_getBrandProducts.arn
    lv_function_name = "Therapist-getBrandProducts"
    lv_Client = "Therapist"
    lv_input = {
      "method.request.querystring.CutitronicsBrandID"=true
    }
}


# getClientPrescriptions


module "module_api_gateway_Therapist_getClientPrescriptions"{
    source            = "./Modules/APIGateway/FullResource"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
    lv_validator_name = module.module_api_gateway_validator.id
    lv_authorization = "COGNITO_USER_POOLS"
    lv_authorization_scopes = ["aws.cognito.signin.user.admin"]    
    lv_execution_arn = module.module_lambda_Cutitronics_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Therapist.id
    lv_part_name = "getClientPrescriptions"
    lv_method = "GET"
    lv_Authorizer = module.module_cognito_therapist_authorizer.id
    lv_arn = module.module_lambda_Therapist_getClientPrescriptions.arn
    lv_function_name = "Therapist-getClientPrescriptions"
    lv_Client = "Therapist"
    lv_input = {
      "method.request.querystring.CutitronicsBrandID"=true,
      "method.request.querystring.CutitronicsClientID"=true
    }
}


# getDefaultPrescriptions


module "module_api_gateway_Therapist_getDefaultPrescriptions"{
    source            = "./Modules/APIGateway/FullResource"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
    lv_validator_name = module.module_api_gateway_validator.id
    lv_authorization = "COGNITO_USER_POOLS"
    lv_authorization_scopes = ["aws.cognito.signin.user.admin"]    
    lv_execution_arn = module.module_lambda_Cutitronics_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Therapist.id
    lv_part_name = "getDefaultPrescriptions"
    lv_method = "GET"
    lv_Authorizer = module.module_cognito_therapist_authorizer.id
    lv_arn = module.module_lambda_Therapist_getDefaultPrescriptions.arn
    lv_function_name = "Therapist-getDefaultPrescriptions"
    lv_Client = "Therapist"
    lv_input = {
      "method.request.querystring.CutitronicsBrandID"=true
    }
}


# getGuest


module "module_api_gateway_Therapist_getGuest"{
    source            = "./Modules/APIGateway/FullResource"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
    lv_validator_name = module.module_api_gateway_validator.id
    lv_authorization = "COGNITO_USER_POOLS"
    lv_authorization_scopes = ["aws.cognito.signin.user.admin"]    
    lv_execution_arn = module.module_lambda_Cutitronics_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Therapist.id
    lv_part_name = "getGuest"
    lv_method = "GET"
    lv_Authorizer = module.module_cognito_therapist_authorizer.id
    lv_arn = module.module_lambda_Therapist_getGuest.arn
    lv_function_name = "Therapist-getGuest"
    lv_Client = "Therapist"
    lv_input = {
      "method.request.querystring.CutitronicsTherapistID"=true,
      "method.request.querystring.CutitronicsBrandID"=true,
      "method.request.querystring.CutitronicsClientID"=true
    }
}


# getSession


module "module_api_gateway_Therapist_getSession"{
    source            = "./Modules/APIGateway/FullResource"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
    lv_validator_name = module.module_api_gateway_validator.id
    lv_authorization = "COGNITO_USER_POOLS"
    lv_authorization_scopes = ["aws.cognito.signin.user.admin"]    
    lv_execution_arn = module.module_lambda_Cutitronics_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Therapist.id
    lv_part_name = "getSession"
    lv_method = "GET"
    lv_Authorizer = module.module_cognito_therapist_authorizer.id
    lv_arn = module.module_lambda_Therapist_getSession.arn
    lv_function_name = "Therapist-getSession"
    lv_Client = "Therapist"
    lv_input = {
      "method.request.querystring.CutitronicsSessionID"=true
    }
}


# getUsage


module "module_api_gateway_Therapist_getUsage"{
    source            = "./Modules/APIGateway/FullResource"
    lv_rest_api_id = module.module_lambda_Cutitronics_Gateway.id
    lv_validator_name = module.module_api_gateway_validator.id
    lv_authorization = "COGNITO_USER_POOLS"
    lv_authorization_scopes = ["aws.cognito.signin.user.admin"]    
    lv_execution_arn = module.module_lambda_Cutitronics_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Therapist.id
    lv_part_name = "getUsage"
    lv_method = "GET"
    lv_Authorizer = module.module_cognito_therapist_authorizer.id
    lv_arn = module.module_lambda_Therapist_getUsage.arn
    lv_function_name = "Therapist-getUsage"
    lv_Client = "Therapist"
    lv_input = {
      "method.request.querystring.CutitronicsClientID"=true
      "method.request.querystring.TreatmentType"=true
    }
}


# Legacy


# Lambdas


# getBrandProducts


module "module_lambda_Legacy_getBrandProducts"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Legacy/getBrandProducts/getBrandProducts.zip"
    lv_function_name = "Legacy-getBrandProducts"
    lv_handler = "getBrandProducts.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# getCartInstall


module "module_lambda_Legacy_getCartInstall"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Legacy/getCartInstall/getCartInstall.zip"
    lv_function_name = "Legacy-getCartInstall"
    lv_handler = "getCartInstall.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# createClient


module "module_lambda_Legacy_createClient"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Legacy/createClient/createClient.zip"
    lv_function_name = "Legacy-createClient"
    lv_handler = "createClient.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# createSession


module "module_lambda_Legacy_createSession"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Legacy/createSession/createSession.zip"
    lv_function_name = "Legacy-createSession"
    lv_handler = "createSession.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# deleteClient


module "module_lambda_Legacy_deleteClient"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Legacy/deleteClient/deleteClient.zip"
    lv_function_name = "Legacy-deleteClient"
    lv_handler = "deleteClient.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# getAllClients


module "module_lambda_Legacy_getAllClients"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Legacy/getAllClients/getAllClients.zip"
    lv_function_name = "Legacy-getAllClients"
    lv_handler = "getAllClients.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# getAllGuests


module "module_lambda_Legacy_getAllGuests"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Legacy/getAllGuests/getAllGuests.zip"
    lv_function_name = "Legacy-getAllGuests"
    lv_handler = "getAllGuests.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# getAllTherapists


module "module_lambda_Legacy_getAllTherapists"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Legacy/getAllTherapists/getAllTherapists.zip"
    lv_function_name = "Legacy-getAllTherapists"
    lv_handler = "getAllTherapists.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# getClient


module "module_lambda_Legacy_getClient"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Legacy/getClient/getClient.zip"
    lv_function_name = "Legacy-getClient"
    lv_handler = "getClient.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# getClientPrescriptions


module "module_lambda_Legacy_getClientPrescriptions"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Legacy/getClientPrescriptions/getClientPrescriptions.zip"
    lv_function_name = "Legacy-getClientPrescriptions"
    lv_handler = "getClientPrescriptions.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# getDefaultPrescriptions


module "module_lambda_Legacy_getDefaultPrescriptions"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Legacy/getDefaultPrescriptions/getDefaultPrescriptions.zip"
    lv_function_name = "Legacy-getDefaultPrescriptions"
    lv_handler = "getDefaultPrescriptions.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# getGuest


module "module_lambda_Legacy_getGuest"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Legacy/getGuest/getGuest.zip"
    lv_function_name = "Legacy-getGuest"
    lv_handler = "getGuest.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# getQuestions


module "module_lambda_Legacy_getQuestions"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Legacy/getQuestions/getQuestions.zip"
    lv_function_name = "Legacy-getQuestions"
    lv_handler = "getQuestions.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# getSession


module "module_lambda_Legacy_getSession"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Legacy/getSession/getSession.zip"
    lv_function_name = "Legacy-getSession"
    lv_handler = "getSession.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# getUsage


module "module_lambda_Legacy_getUsage"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Legacy/getUsage/getUsage.zip"
    lv_function_name = "Legacy-getUsage"
    lv_handler = "getUsage.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# logCartInstall


module "module_lambda_Legacy_logCartInstall"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Legacy/logCartInstall/logCartInstall.zip"
    lv_function_name = "Legacy-logCartInstall"
    lv_handler = "logCartInstall.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# postAnswers


module "module_lambda_Legacy_postAnswers"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Legacy/postAnswers/postAnswers.zip"
    lv_function_name = "Legacy-postAnswers"
    lv_handler = "postAnswers.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# postGuestNotes


module "module_lambda_Legacy_postGuestNotes"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Legacy/postGuestNotes/postGuestNotes.zip"
    lv_function_name = "Legacy-postGuestNotes"
    lv_handler = "postGuestNotes.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# reqFirmwareUpdate


module "module_lambda_Legacy_reqFirmwareUpdate"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Legacy/reqFirmwareUpdate/reqFirmwareUpdate.zip"
    lv_function_name = "Legacy-reqFirmwareUpdate"
    lv_handler = "reqFirmwareUpdate.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# treatmentTypes

module "module_lambda_Legacy_treatmentTypes"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Legacy/treatmentTypes/postTreatmentTypes.zip"
    lv_function_name = "Legacy-treatmentTypes"
    lv_handler = "postTreatmentTypes.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# updateSession

module "module_lambda_Legacy_updateSession"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Legacy/updateSession/updateSession.zip"
    lv_function_name = "Legacy-updateSession"
    lv_handler = "updateSession.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}


# uploadUsage


module "module_lambda_Legacy_uploadUsage"{
    source            = "./Modules/Lambda/Function"
    lv_filename = "./Source_Files/Lambda_Source/Legacy/uploadUsage/uploadUsage.zip"
    lv_function_name = "Legacy-uploadUsage"
    lv_handler = "uploadUsage.lambda_handler"
    lv_description = "Desc"
    lv_runtime = "python3.8"
    lv_memory_size = 128
    lv_timeout = 10
    lv_layers = "${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_layer_arn}:${module.module_lambda_rds_layer.aws_lambda_layer_version_pymysql_version}"
    lv_rds_username = var.v_rds_username
    lv_rds_password = var.v_rds_password
    lv_rds_host = module.module_create_cutitronics_rds.cutitronics_MariaDB_address
    lv_rds_name = var.v_rds_name
    v_environment = "${var.v_client}_${var.v_environment}"
}

# API

module "module_api_gateway_resource_Legacy_ClientDetails"{
    source            = "./Modules/APIGateway/Resources"
    lv_rest_api_id = module.module_lambda_Cutitronics_legacy_Gateway.id
    lv_parent_id = module.module_lambda_Cutitronics_legacy_Gateway.root_resource_id
    lv_part_name = "clientdetails"
}


module "module_api_gateway_resource_Legacy_Therapist"{
    source            = "./Modules/APIGateway/Resources"
    lv_rest_api_id = module.module_lambda_Cutitronics_legacy_Gateway.id
    lv_parent_id = module.module_lambda_Cutitronics_legacy_Gateway.root_resource_id
    lv_part_name = "Therapist"
}


module "module_api_gateway_resource_Legacy_xDesign"{
    source            = "./Modules/APIGateway/Resources"
    lv_rest_api_id = module.module_lambda_Cutitronics_legacy_Gateway.id
    lv_parent_id = module.module_lambda_Cutitronics_legacy_Gateway.root_resource_id
    lv_part_name = "xDesign"
}


module "module_api_gateway_resource_Legacy_ClientSurvey"{
    source            = "./Modules/APIGateway/Resources"
    lv_rest_api_id = module.module_lambda_Cutitronics_legacy_Gateway.id
    lv_parent_id = module.module_api_gateway_resource_Legacy_xDesign.id
    lv_part_name = "ClientSurvey"
}



# Create Cutitronics Validator


module "module_api_gateway_legacy_validator"{
    source            = "./Modules/APIGateway/Validator"
    lv_validator_name = "CutitronicsValidator"
    lv_rest_api_id = module.module_lambda_Cutitronics_legacy_Gateway.id
}


# Full Legacy API


# updateSession


module "module_api_gateway_Legacy_updateSession"{
    source            = "./Modules/APIGateway/FullResourceNoAuth"
    lv_rest_api_id = module.module_lambda_Cutitronics_legacy_Gateway.id
    lv_validator_name = module.module_api_gateway_legacy_validator.id
    lv_authorization = "NONE"
    lv_execution_arn = module.module_lambda_Cutitronics_legacy_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Legacy_xDesign.id
    lv_part_name = "updateSession"
    lv_method = "POST"
    lv_arn = module.module_lambda_Legacy_updateSession.arn
    lv_function_name = "Legacy-updateSession"
    lv_Client = "xDesign/updateSession"
    # lv_Client = module.module_lambda_Cutitronics_legacy_Gateway.execution_arn"/*/POST/updateSession"
}


# getGuest /xDesign/getGuest POST  


module "module_api_gateway_Legacy_getGuest"{
    source            = "./Modules/APIGateway/FullResourceNoAuth"
    lv_rest_api_id = module.module_lambda_Cutitronics_legacy_Gateway.id
    lv_validator_name = module.module_api_gateway_legacy_validator.id
    lv_authorization = "NONE"
    lv_execution_arn = module.module_lambda_Cutitronics_legacy_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Legacy_xDesign.id
    lv_part_name = "getGuest"
    lv_method = "POST"
    lv_arn = module.module_lambda_Legacy_getGuest.arn
    lv_function_name = "Legacy-getGuest"
    lv_Client = "xDesign/getGuest"
}


# createSession /xDesign/createSession POST


module "module_api_gateway_Legacy_createSession"{
    source            = "./Modules/APIGateway/FullResourceNoAuth"
    lv_rest_api_id = module.module_lambda_Cutitronics_legacy_Gateway.id
    lv_validator_name = module.module_api_gateway_legacy_validator.id
    lv_authorization = "NONE"
    lv_execution_arn = module.module_lambda_Cutitronics_legacy_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Legacy_xDesign.id
    lv_part_name = "createSession"
    lv_method = "POST"
    lv_arn = module.module_lambda_Legacy_createSession.arn
    lv_function_name = "Legacy-createSession"
    lv_Client = "xDesign/createSession"
}


# getAllGuests /xDesign/getAllGuests POST


module "module_api_gateway_Legacy_getAllGuests"{
    source            = "./Modules/APIGateway/FullResourceNoAuth"
    lv_rest_api_id = module.module_lambda_Cutitronics_legacy_Gateway.id
    lv_validator_name = module.module_api_gateway_legacy_validator.id
    lv_authorization = "NONE"
    lv_execution_arn = module.module_lambda_Cutitronics_legacy_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Legacy_xDesign.id
    lv_part_name = "getAllGuests"
    lv_method = "POST"
    lv_arn = module.module_lambda_Legacy_getAllGuests.arn
    lv_function_name = "Legacy-getAllGuests"
    lv_Client = "xDesign/getAllGuests"
}


# postGuestNotes /xDesign/postGuestNotes POST


module "module_api_gateway_Legacy_postGuestNotes"{
    source            = "./Modules/APIGateway/FullResourceNoAuth"
    lv_rest_api_id = module.module_lambda_Cutitronics_legacy_Gateway.id
    lv_validator_name = module.module_api_gateway_legacy_validator.id
    lv_authorization = "NONE"
    lv_execution_arn = module.module_lambda_Cutitronics_legacy_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Legacy_xDesign.id
    lv_part_name = "postGuestNotes"
    lv_method = "POST"
    lv_arn = module.module_lambda_Legacy_postGuestNotes.arn
    lv_function_name = "Legacy-postGuestNotes"
    lv_Client = "xDesign/postGuestNotes"
}


# treatmentTypes /xDesign/treatmentTypes POST


module "module_api_gateway_Legacy_treatmentTypes"{
    source            = "./Modules/APIGateway/FullResourceNoAuth"
    lv_rest_api_id = module.module_lambda_Cutitronics_legacy_Gateway.id
    lv_validator_name = module.module_api_gateway_legacy_validator.id
    lv_authorization = "NONE"
    lv_execution_arn = module.module_lambda_Cutitronics_legacy_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Legacy_xDesign.id
    lv_part_name = "treatmentTypes"
    lv_method = "POST"
    lv_arn = module.module_lambda_Legacy_treatmentTypes.arn
    lv_function_name = "Legacy-treatmentTypes"
    lv_Client = "xDesign/treatmentTypes"
}


# getClientPrescriptions /xDesign/getClientPrescriptions GET


module "module_api_gateway_legacy_getClientPrescriptions"{
    source            = "./Modules/APIGateway/FullResourceNoAuth"
    lv_rest_api_id = module.module_lambda_Cutitronics_legacy_Gateway.id
    lv_validator_name = module.module_api_gateway_legacy_validator.id
    lv_authorization = "NONE"
    lv_execution_arn = module.module_lambda_Cutitronics_legacy_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Legacy_xDesign.id
    lv_part_name = "getClientPrescriptions"
    lv_method = "GET"
    lv_arn = module.module_lambda_Legacy_getClientPrescriptions.arn
    lv_function_name = "Legacy-getClientPrescriptions"
    lv_Client = "xDesign/getClientPrescriptions"
    lv_input = {
      "method.request.querystring.CutitronicsBrandID"=true
      "method.request.querystring.CutitronicsClientID"=true
    }
}


# getUsage /xDesign/getUsage GETT


module "module_api_gateway_legacy_getUsage"{
    source            = "./Modules/APIGateway/FullResourceNoAuth"
    lv_rest_api_id = module.module_lambda_Cutitronics_legacy_Gateway.id
    lv_validator_name = module.module_api_gateway_legacy_validator.id
    lv_authorization = "NONE"
    lv_execution_arn = module.module_lambda_Cutitronics_legacy_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Legacy_xDesign.id
    lv_part_name = "getUsage"
    lv_method = "GET"
    lv_arn = module.module_lambda_Legacy_getUsage.arn
    lv_function_name = "Legacy-getUsage"
    lv_Client = "xDesign/getUsage"
    lv_input = {
      "method.request.querystring.CutitronicsClientID"=true
      "method.request.querystring.TreatmentType"=true
    }
}


# getBrandProducts /xDesign/getBrandProducts GET


module "module_api_gateway_legacy_getBrandProducts"{
    source            = "./Modules/APIGateway/FullResourceNoAuth"
    lv_rest_api_id = module.module_lambda_Cutitronics_legacy_Gateway.id
    lv_validator_name = module.module_api_gateway_legacy_validator.id
    lv_authorization = "NONE"
    lv_execution_arn = module.module_lambda_Cutitronics_legacy_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Legacy_xDesign.id
    lv_part_name = "getBrandProducts"
    lv_method = "GET"
    lv_arn = module.module_lambda_Legacy_getBrandProducts.arn
    lv_function_name = "Legacy-getBrandProducts"
    lv_Client = "xDesign/getBrandProducts"
    lv_input = {
      "method.request.querystring.CutitronicsBrandID"=true
    }
}


# getDefaultPrescriptions /xDesign/getDefaultPrescriptions GET


module "module_api_gateway_legacy_getDefaultPrescriptions"{
    source            = "./Modules/APIGateway/FullResourceNoAuth"
    lv_rest_api_id = module.module_lambda_Cutitronics_legacy_Gateway.id
    lv_validator_name = module.module_api_gateway_legacy_validator.id
    lv_authorization = "NONE"
    lv_execution_arn = module.module_lambda_Cutitronics_legacy_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Legacy_xDesign.id
    lv_part_name = "getDefaultPrescriptions"
    lv_method = "GET"
    lv_arn = module.module_lambda_Legacy_getDefaultPrescriptions.arn
    lv_function_name = "Legacy-getDefaultPrescriptions"
    lv_Client = "xDesign/getDefaultPrescriptions"
    lv_input = {
      "method.request.querystring.CutitronicsBrandID"=true
    }
}


# getSession /xDesign/getSession GET


module "module_api_gateway_legacy_getSession"{
    source            = "./Modules/APIGateway/FullResourceNoAuth"
    lv_rest_api_id = module.module_lambda_Cutitronics_legacy_Gateway.id
    lv_validator_name = module.module_api_gateway_legacy_validator.id
    lv_authorization = "NONE"
    lv_execution_arn = module.module_lambda_Cutitronics_legacy_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Legacy_xDesign.id
    lv_part_name = "getSession"
    lv_method = "GET"
    lv_arn = module.module_lambda_Legacy_getSession.arn
    lv_function_name = "Legacy-getSession"
    lv_Client = "xDesign/getSession"
    lv_input = {
      "method.request.querystring.CutitronicsSessionID"=true
    }
}


# getAllClients /xDesign/getAllClients GET


module "module_api_gateway_legacy_getAllClients"{
    source            = "./Modules/APIGateway/FullResourceNoAuth"
    lv_rest_api_id = module.module_lambda_Cutitronics_legacy_Gateway.id
    lv_validator_name = module.module_api_gateway_legacy_validator.id
    lv_authorization = "NONE"
    lv_execution_arn = module.module_lambda_Cutitronics_legacy_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Legacy_xDesign.id
    lv_part_name = "getAllClients"
    lv_method = "GET"
    lv_arn = module.module_lambda_Legacy_getAllClients.arn
    lv_function_name = "Legacy-getAllClients"
    lv_Client = "xDesign/getAllClients"
    lv_input = {
      "method.request.querystring.CutitronicsBrandID"=true
    }
}


# getAllTherapists /xDesign/getAllTherapists GET


module "module_api_gateway_legacy_getAllTherapists"{
    source            = "./Modules/APIGateway/FullResourceNoAuth"
    lv_rest_api_id = module.module_lambda_Cutitronics_legacy_Gateway.id
    lv_validator_name = module.module_api_gateway_legacy_validator.id
    lv_authorization = "NONE"
    lv_execution_arn = module.module_lambda_Cutitronics_legacy_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Legacy_xDesign.id
    lv_part_name = "getAllTherapists"
    lv_method = "GET"
    lv_arn = module.module_lambda_Legacy_getAllTherapists.arn
    lv_function_name = "Legacy-getAllTherapists"
    lv_Client = "xDesign/getAllTherapists"
    lv_input = {
      "method.request.querystring.CutitronicsBrandID"=true
    }
}


# getCartInstall /xDesign/getCartInstall GET


module "module_api_gateway_legacy_getCartInstall"{
    source            = "./Modules/APIGateway/FullResourceNoAuth"
    lv_rest_api_id = module.module_lambda_Cutitronics_legacy_Gateway.id
    lv_validator_name = module.module_api_gateway_legacy_validator.id
    lv_authorization = "NONE"
    lv_execution_arn = module.module_lambda_Cutitronics_legacy_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Legacy_xDesign.id
    lv_part_name = "getCartInstall"
    lv_method = "GET"
    lv_arn = module.module_lambda_Legacy_getCartInstall.arn
    lv_function_name = "Legacy-getCartInstall"
    lv_Client = "xDesign/getCartInstall"
    lv_input = {
      "method.request.querystring.CutitronicsCartID"=true
    }
}


# getClient /xDesign/getClient GET


module "module_api_gateway_legacy_getClient"{
    source            = "./Modules/APIGateway/FullResourceNoAuth"
    lv_rest_api_id = module.module_lambda_Cutitronics_legacy_Gateway.id
    lv_validator_name = module.module_api_gateway_legacy_validator.id
    lv_authorization = "NONE"
    lv_execution_arn = module.module_lambda_Cutitronics_legacy_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Legacy_xDesign.id
    lv_part_name = "getClient"
    lv_method = "GET"
    lv_arn = module.module_lambda_Legacy_getClient.arn
    lv_function_name = "Legacy-getClient"
    lv_Client = "xDesign/getClient"
    lv_input = {
      "method.request.querystring.CutitronicsClientID"=true
    }
}


# getQuestions /xDesign/ClientSurvey/getQuestions GET


module "module_api_gateway_legacy_getQuestions"{
    source            = "./Modules/APIGateway/FullResourceNoAuth"
    lv_rest_api_id = module.module_lambda_Cutitronics_legacy_Gateway.id
    lv_validator_name = module.module_api_gateway_legacy_validator.id
    lv_authorization = "NONE"
    lv_execution_arn = module.module_lambda_Cutitronics_legacy_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Legacy_ClientSurvey.id
    lv_part_name = "getQuestions"
    lv_method = "GET"
    lv_arn = module.module_lambda_Legacy_getQuestions.arn
    lv_function_name = "Legacy-getQuestions"
    lv_Client = "xDesign/ClientSurvey/getQuestions"
    lv_input = {
      "method.request.querystring.CutitronicsBrandID"=true,
      "method.request.querystring.QuestionSet"=true
    }
}


# postAnswers /xDesign/ClientSurvey/postAnswers POST


module "module_api_gateway_Legacy_postAnswers"{
    source            = "./Modules/APIGateway/FullResourceNoAuth"
    lv_rest_api_id = module.module_lambda_Cutitronics_legacy_Gateway.id
    lv_validator_name = module.module_api_gateway_legacy_validator.id
    lv_authorization = "NONE"
    lv_execution_arn = module.module_lambda_Cutitronics_legacy_Gateway.execution_arn
    lv_parent_id = module.module_api_gateway_resource_Legacy_ClientSurvey.id
    lv_part_name = "postAnswers"
    lv_method = "POST"
    lv_arn = module.module_lambda_Legacy_postAnswers.arn
    lv_function_name = "Legacy-postAnswers"
    lv_Client = "xDesign/ClientSurvey/postAnswers" 
}


# reqFirmwareUpdate /reqFirmwareUpdate POST


module "module_api_gateway_Legacy_reqFirmwareUpdate"{
    source            = "./Modules/APIGateway/FullResourceNoAuth"
    lv_rest_api_id = module.module_lambda_Cutitronics_legacy_Gateway.id
    lv_validator_name = module.module_api_gateway_legacy_validator.id
    lv_authorization = "NONE"
    lv_execution_arn = module.module_lambda_Cutitronics_legacy_Gateway.execution_arn
    lv_parent_id = module.module_lambda_Cutitronics_legacy_Gateway.root_resource_id
    lv_part_name = "reqFirmwareUpdate"
    lv_method = "POST"
    lv_arn = module.module_lambda_Legacy_reqFirmwareUpdate.arn
    lv_function_name = "Legacy-reqFirmwareUpdate"
    lv_Client = "reqFirmwareUpdate"
}

# createClient /createClient POST


module "module_api_gateway_Legacy_createClient"{
    source            = "./Modules/APIGateway/FullResourceNoAuth"
    lv_rest_api_id = module.module_lambda_Cutitronics_legacy_Gateway.id
    lv_validator_name = module.module_api_gateway_legacy_validator.id
    lv_authorization = "NONE"
    lv_execution_arn = module.module_lambda_Cutitronics_legacy_Gateway.execution_arn
    lv_parent_id = module.module_lambda_Cutitronics_legacy_Gateway.root_resource_id
    lv_part_name = "createClient"
    lv_method = "POST"
    lv_arn = module.module_lambda_Legacy_createClient.arn
    lv_function_name = "Legacy-createClient"
    lv_Client = "createClient"
}


# deleteClient /deleteClient POST


module "module_api_gateway_Legacy_deleteClient"{
    source            = "./Modules/APIGateway/FullResourceNoAuth"
    lv_rest_api_id = module.module_lambda_Cutitronics_legacy_Gateway.id
    lv_validator_name = module.module_api_gateway_legacy_validator.id
    lv_authorization = "NONE"
    lv_execution_arn = module.module_lambda_Cutitronics_legacy_Gateway.execution_arn
    lv_parent_id = module.module_lambda_Cutitronics_legacy_Gateway.root_resource_id
    lv_part_name = "deleteClient"
    lv_method = "POST"
    lv_arn = module.module_lambda_Legacy_deleteClient.arn
    lv_function_name = "Legacy-deleteClient"
    lv_Client = "deleteClient"
}


# logCartInstall /logCartInstall POST


module "module_api_gateway_Legacy_logCartInstall"{
    source            = "./Modules/APIGateway/FullResourceNoAuth"
    lv_rest_api_id = module.module_lambda_Cutitronics_legacy_Gateway.id
    lv_validator_name = module.module_api_gateway_legacy_validator.id
    lv_authorization = "NONE"
    lv_execution_arn = module.module_lambda_Cutitronics_legacy_Gateway.execution_arn
    lv_parent_id = module.module_lambda_Cutitronics_legacy_Gateway.root_resource_id
    lv_part_name = "logCartInstall"
    lv_method = "POST"
    lv_arn = module.module_lambda_Legacy_logCartInstall.arn
    lv_function_name = "Legacy-logCartInstall"
    lv_Client = "logCartInstall" 
}


# uploadUsage /uploadUsage POST


module "module_api_gateway_Legacy_uploadUsage"{
    source            = "./Modules/APIGateway/FullResourceNoAuth"
    lv_rest_api_id = module.module_lambda_Cutitronics_legacy_Gateway.id
    lv_validator_name = module.module_api_gateway_legacy_validator.id
    lv_authorization = "NONE"
    lv_execution_arn = module.module_lambda_Cutitronics_legacy_Gateway.execution_arn
    lv_parent_id = module.module_lambda_Cutitronics_legacy_Gateway.root_resource_id
    lv_part_name = "uploadUsage"
    lv_method = "POST"
    lv_arn = module.module_lambda_Legacy_uploadUsage.arn
    lv_function_name = "Legacy-uploadUsage"
    lv_Client = "uploadUsage"
} 

# Client  Dashboards

module "create_cutitronics_ops_dashboards_client_dashboards" {
    source = "./Modules/Cloudwatch-Dashboards/Dashboards-FullResources"
    
    lv_dashboards_user =  "Client"

    # ${module.module_create_cutitronics_rds.name}

    lv_database_name = "${var.v_rds_name}"
 
}

   

# Legacy  Dashboards

module "create_cutitronics_ops_dashboards_client_dashboards_apigateway" {
    source = "./Modules/Cloudwatch-Dashboards/Dashboards-FullResources"
    
    lv_dashboards_user =  "Legacy"

    # ${module.module_create_cutitronics_rds.name}

    lv_database_name = "${var.v_rds_name}"
 
}

   

# Therapist  Dashboards

module "create_cutitronics_ops_dashboards_client_dashboards_rds" {
    source = "./Modules/Cloudwatch-Dashboards/Dashboards-FullResources"
    
    lv_dashboards_user =  "Therapist"

    # ${module.module_create_cutitronics_rds.name}

    lv_database_name = "${var.v_rds_name}"
 
}

   