resource "aws_api_gateway_authorizer" "Authorizer" {
  name          = var.lv_name
  rest_api_id   = var.lv_rest_api
  type          = "COGNITO_USER_POOLS"
  provider_arns = [var.lv_provider]
}