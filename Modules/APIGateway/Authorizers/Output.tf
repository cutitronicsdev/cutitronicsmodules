output "id" {
  description = "The id of the user pool"
  value       = aws_api_gateway_authorizer.Authorizer.id
}
