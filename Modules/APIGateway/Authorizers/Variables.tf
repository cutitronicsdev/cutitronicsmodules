variable "lv_name" {
  type        = string
  description = "(Required) The name of the user pool."
}


variable "lv_provider" {
  type        = string
}


variable "lv_rest_api" {
  type        = string
}
