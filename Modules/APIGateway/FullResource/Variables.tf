variable "lv_rest_api_id" {
  type        = string
}


variable "lv_parent_id" {
  type        = string
}


variable "lv_part_name" {
  type        = string
}


variable "lv_method" {
  type        = string
}


variable "lv_Authorizer" {
  type        = string
  default = ""
}


variable "lv_arn" {
  type        = string
}

variable "lv_function_name"{
  type        = string
}


variable "lv_execution_arn"{
  type        = string
}


variable "lv_Client"{
  type        = string
}


variable "lv_input"{
  default = {}
  type = map
}


variable "lv_validator_name"{
  type        = string
}


variable "lv_authorization" {
  type        = string
  default = ""
}


variable "lv_authorization_scopes"{
  type    = list(string)
}