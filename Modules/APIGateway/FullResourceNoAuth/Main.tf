
resource "aws_api_gateway_resource" "resource" {
  rest_api_id = var.lv_rest_api_id
  parent_id   = var.lv_parent_id
  path_part   = var.lv_part_name
}


resource "aws_api_gateway_method" "method" {
  rest_api_id = var.lv_rest_api_id
  resource_id   = aws_api_gateway_resource.resource.id
  http_method   = var.lv_method
  authorization = var.lv_authorization
  # authorizer_id = var.lv_Authorizer
  # authorization_scopes  = var.lv_authorization_scopes
  request_parameters = var.lv_input
  request_validator_id = var.lv_validator_name
}


resource "aws_api_gateway_integration" "Integration" {
  rest_api_id = var.lv_rest_api_id
  resource_id   = aws_api_gateway_resource.resource.id
  http_method = aws_api_gateway_method.method.http_method
  type        = "AWS_PROXY"
  uri         = "arn:aws:apigateway:eu-west-2:lambda:path/2015-03-31/functions/${var.lv_arn}/invocations"
  integration_http_method = "POST"
}


resource "aws_api_gateway_method_response" "http_response" {
  rest_api_id = var.lv_rest_api_id
  resource_id   = aws_api_gateway_resource.resource.id
  http_method = aws_api_gateway_method.method.http_method
  status_code = "200"  
  response_models = {
      "application/json" = "Empty"
    }  
}



resource "aws_api_gateway_integration_response" "Response" {
  rest_api_id = var.lv_rest_api_id
  resource_id   = aws_api_gateway_resource.resource.id
  http_method = aws_api_gateway_method.method.http_method  
  status_code = aws_api_gateway_method_response.http_response.status_code
  depends_on = ["aws_api_gateway_integration.Integration", "aws_api_gateway_method_response.http_response"]
}



# resource "aws_api_gateway_request_validator" "validator" {
#   name                        = var.lv_function_name
#   rest_api_id = var.lv_rest_api_id
#   validate_request_body       = true
#   validate_request_parameters = true
# }


resource "aws_lambda_permission" "permission" {
  statement_id  = "${var.lv_function_name}_permission"
  action        = "lambda:InvokeFunction"
  function_name = var.lv_function_name
  principal     = "apigateway.amazonaws.com"
  # source_arn = "${var.lv_execution_arn}/*/${var.lv_method}/${var.lv_Client}/${var.lv_part_name}" 
  source_arn = "${var.lv_execution_arn}/*/${var.lv_method}/${var.lv_Client}" 
  depends_on    = ["aws_api_gateway_resource.resource", "aws_api_gateway_integration.Integration"]
}