output "id" {
  description = "The id of the user pool"
  value       = aws_api_gateway_rest_api.rest_api.id
}


output "arn" {
  description = "The ARN of the user pool."
  value       = aws_api_gateway_rest_api.rest_api.arn
}


output "root_resource_id" {
  description = "The ARN of the user pool."
  value       = aws_api_gateway_rest_api.rest_api.root_resource_id
}

output "execution_arn"{
  value       = aws_api_gateway_rest_api.rest_api.execution_arn
}