
resource "aws_api_gateway_resource" "resource" {
  rest_api_id = var.lv_rest_api_id
  parent_id   = var.lv_parent_id
  path_part   = var.lv_part_name
}