resource "aws_api_gateway_request_validator" "validator" {
  name                        = var.lv_validator_name
  rest_api_id                 = var.lv_rest_api_id
  validate_request_body       = true
  validate_request_parameters = true
}