

resource "aws_cloudwatch_dashboard" "Cutitronics-Lambdas" {

   dashboard_name = "${var.lv_dashboards_user}-Lambdas"
   
   
 dashboard_body = <<EOF
 {
   "widgets": [
       {
          "type":"metric",
          "x":0,
          "y":0,
          "width":12,
          "height":6,
          "properties":{
            "metrics": [
                [ "AWS/Lambda", "Invocations", "FunctionName", "${var.lv_dashboards_user}-getDefaultPrescriptions", { "stat": "Sum", "period": 2592000 } ],
                [ ".", "Throttles", ".", ".", { "period": 2592000 } ],
                [ ".", "Duration", ".", ".", { "period": 2592000 } ],
                [ ".", "Errors", ".", ".", { "period": 2592000 } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-getDefaultPrescriptions",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-P7D",
             "end": "P0D"             
          }
       },
         {
          "type":"metric",
          "x":23,
          "y":0,
          "width":12,
          "height":6,
          "properties":{
            "metrics": [
                [ "AWS/Lambda", "Invocations", "FunctionName", "${var.lv_dashboards_user}-getUsage", { "stat": "Sum", "period": 2592000 } ],
                [ ".", "Throttles", ".", ".", { "period": 2592000 } ],
                [ ".", "Duration", ".", ".", { "period": 2592000 } ],
                [ ".", "Errors", ".", ".", { "period": 2592000 } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-getUsage",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-P7D",
             "end": "P0D"             
          }          
       },
         {
          "type":"metric",
          "x":23,
          "y":0,
          "width":12,
          "height":6,
          "properties":{
            "metrics": [
                [ "AWS/Lambda", "Invocations", "FunctionName", "${var.lv_dashboards_user}-reqFirmwareUpdate", { "stat": "Sum", "period": 2592000 } ],
                [ ".", "Throttles", ".", ".", { "period": 2592000 } ],
                [ ".", "Duration", ".", ".", { "period": 2592000 } ],
                [ ".", "Errors", ".", ".", { "period": 2592000 } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-reqFirmwareUpdate",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-P7D",
             "end": "P0D"             
          }          
        },
             {
          "type":"metric",
          "x":0,
          "y":250,
          "width":12,
          "height":6,
          "properties":{
            "metrics": [
                [ "AWS/Lambda", "Invocations", "FunctionName", "${var.lv_dashboards_user}-createSession", { "stat": "Sum", "period": 2592000 } ],
                [ ".", "Throttles", ".", ".", { "period": 2592000 } ],
                [ ".", "Duration", ".", ".", { "period": 2592000 } ],
                [ ".", "Errors", ".", ".", { "period": 2592000 } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-createSession",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-P7D",
             "end": "P0D"             
          }          
       }, 
         {
          "type":"metric",
          "x":23,
          "y":250,
          "width":12,
          "height":6,
          "properties":{
            "metrics": [
                [ "AWS/Lambda", "Invocations", "FunctionName", "${var.lv_dashboards_user}-getSession", { "stat": "Sum", "period": 2592000 } ],
                [ ".", "Throttles", ".", ".", { "period": 2592000 } ],
                [ ".", "Duration", ".", ".", { "period": 2592000 } ],
                [ ".", "Errors", ".", ".", { "period": 2592000 } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-getSession",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-P7D",
             "end": "P0D"             
          }          
       },
         {
          "type":"metric",
          "x":0,
          "y":500,
          "width":12,
          "height":6,
          "properties":{
            "metrics": [
                [ "AWS/Lambda", "Invocations", "FunctionName", "${var.lv_dashboards_user}-deleteClient", { "stat": "Sum", "period": 2592000 } ],
                [ ".", "Throttles", ".", ".", { "period": 2592000 } ],
                [ ".", "Duration", ".", ".", { "period": 2592000 } ],
                [ ".", "Errors", ".", ".", { "period": 2592000 } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-deleteClient",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-P7D",
             "end": "P0D"             
          }          
       },
       
       {
          "type":"metric",
          "x":23,
          "y":500,
          "width":12,
          "height":6,
          "properties":{
            "metrics": [
                [ "AWS/Lambda", "Invocations", "FunctionName", "${var.lv_dashboards_user}-createClient", { "stat": "Sum", "period": 2592000 } ],
                [ ".", "Throttles", ".", ".", { "period": 2592000 } ],
                [ ".", "Duration", ".", ".", { "period": 2592000 } ],
                [ ".", "Errors", ".", ".", { "period": 2592000 } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-createClient",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-P7D",
             "end": "P0D"             
          }          
       },
           {
          "type":"metric",
          "x":0,
          "y":750,
          "width":12,
          "height":6,
          "properties":{
            "metrics": [
                [ "AWS/Lambda", "Invocations", "FunctionName", "${var.lv_dashboards_user}-postAnswers", { "stat": "Sum", "period": 2592000 } ],
                [ ".", "Throttles", ".", ".", { "period": 2592000 } ],
                [ ".", "Duration", ".", ".", { "period": 2592000 } ],
                [ ".", "Errors", ".", ".", { "period": 2592000 } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-postAnswers",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-P7D",
             "end": "P0D"             
          }          
       },

          {
          "type":"metric",
          "x":23,
          "y":750,
          "width":12,
          "height":6,
          "properties":{
            "metrics": [
                [ "AWS/Lambda", "Invocations", "FunctionName", "${var.lv_dashboards_user}-updateSession", { "stat": "Sum", "period": 2592000 } ],
                [ ".", "Throttles", ".", ".", { "period": 2592000 } ],
                [ ".", "Duration", ".", ".", { "period": 2592000 } ],
                [ ".", "Errors", ".", ".", { "period": 2592000 } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-updateSession",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-P7D",
             "end": "P0D"             
          }          
       },
         {
          "type":"metric",
          "x":23,
          "y":750,
          "width":12,
          "height":6,
          "properties":{
            "metrics": [
                [ "AWS/Lambda", "Invocations", "FunctionName", "${var.lv_dashboards_user}-postGuestNotes", { "stat": "Sum", "period": 2592000 } ],
                [ ".", "Throttles", ".", ".", { "period": 2592000 } ],
                [ ".", "Duration", ".", ".", { "period": 2592000 } ],
                [ ".", "Errors", ".", ".", { "period": 2592000 } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-postGuestNotes",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-P7D",
             "end": "P0D"             
          }          
       },
         {
          "type":"metric",
          "x":23,
          "y":750,
          "width":12,
          "height":6,
          "properties":{
            "metrics": [
                [ "AWS/Lambda", "Invocations", "FunctionName", "${var.lv_dashboards_user}-getGuest", { "stat": "Sum", "period": 2592000 } ],
                [ ".", "Throttles", ".", ".", { "period": 2592000 } ],
                [ ".", "Duration", ".", ".", { "period": 2592000 } ],
                [ ".", "Errors", ".", ".", { "period": 2592000 } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-getGuest",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-P7D",
             "end": "P0D"             
          }          
       },
        {
          "type":"metric",
          "x":23,
          "y":750,
          "width":12,
          "height":6,
          "properties":{
            "metrics": [
                [ "AWS/Lambda", "Invocations", "FunctionName", "${var.lv_dashboards_user}-getClient", { "stat": "Sum", "period": 2592000 } ],
                [ ".", "Throttles", ".", ".", { "period": 2592000 } ],
                [ ".", "Duration", ".", ".", { "period": 2592000 } ],
                [ ".", "Errors", ".", ".", { "period": 2592000 } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-getClient",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-P7D",
             "end": "P0D"             
          }          
       },
         {
          "type":"metric",
          "x":23,
          "y":750,
          "width":12,
          "height":6,
          "properties":{
            "metrics": [
                [ "AWS/Lambda", "Invocations", "FunctionName", "${var.lv_dashboards_user}-getAllTherapists", { "stat": "Sum", "period": 2592000 } ],
                [ ".", "Throttles", ".", ".", { "period": 2592000 } ],
                [ ".", "Duration", ".", ".", { "period": 2592000 } ],
                [ ".", "Errors", ".", ".", { "period": 2592000 } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-getAllTherapists",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-P7D",
             "end": "P0D"             
          }          
       },
          {
          "type":"metric",
          "x":23,
          "y":750,
          "width":12,
          "height":6,
          "properties":{
            "metrics": [
                [ "AWS/Lambda", "Invocations", "FunctionName", "${var.lv_dashboards_user}-logCartInstall", { "stat": "Sum", "period": 2592000 } ],
                [ ".", "Throttles", ".", ".", { "period": 2592000 } ],
                [ ".", "Duration", ".", ".", { "period": 2592000 } ],
                [ ".", "Errors", ".", ".", { "period": 2592000 } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-logCartInstall",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-P7D",
             "end": "P0D"             
          }          
       },
        {
          "type":"metric",
          "x":23,
          "y":750,
          "width":12,
          "height":6,
          "properties":{
            "metrics": [
                [ "AWS/Lambda", "Invocations", "FunctionName", "${var.lv_dashboards_user}-uploadUsage", { "stat": "Sum", "period": 2592000 } ],
                [ ".", "Throttles", ".", ".", { "period": 2592000 } ],
                [ ".", "Duration", ".", ".", { "period": 2592000 } ],
                [ ".", "Errors", ".", ".", { "period": 2592000 } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-uploadUsage",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-P7D",
             "end": "P0D"             
          }          
       },
        {
          "type":"metric",
          "x":23,
          "y":750,
          "width":12,
          "height":6,
          "properties":{
            "metrics": [
                [ "AWS/Lambda", "Invocations", "FunctionName", "${var.lv_dashboards_user}-getAllClients", { "stat": "Sum", "period": 2592000 } ],
                [ ".", "Throttles", ".", ".", { "period": 2592000 } ],
                [ ".", "Duration", ".", ".", { "period": 2592000 } ],
                [ ".", "Errors", ".", ".", { "period": 2592000 } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-getAllClients",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-P7D",
             "end": "P0D"             
          }          
       },
        {
          "type":"metric",
          "x":23,
          "y":750,
          "width":12,
          "height":6,
          "properties":{
            "metrics": [
                [ "AWS/Lambda", "Invocations", "FunctionName", "${var.lv_dashboards_user}-treatmentTypes", { "stat": "Sum", "period": 2592000 } ],
                [ ".", "Throttles", ".", ".", { "period": 2592000 } ],
                [ ".", "Duration", ".", ".", { "period": 2592000 } ],
                [ ".", "Errors", ".", ".", { "period": 2592000 } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-treatmentTypes",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-P7D",
             "end": "P0D"             
          }          
        },
        {
          "type":"metric",
          "x":23,
          "y":750,
          "width":12,
          "height":6,
          "properties":{
            "metrics": [
                [ "AWS/Lambda", "Invocations", "FunctionName", "${var.lv_dashboards_user}-getAllGuests", { "stat": "Sum", "period": 2592000 } ],
                [ ".", "Throttles", ".", ".", { "period": 2592000 } ],
                [ ".", "Duration", ".", ".", { "period": 2592000 } ],
                [ ".", "Errors", ".", ".", { "period": 2592000 } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-getAllGuests",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-P7D",
             "end": "P0D"             
          }          
       },
        {
          "type":"metric",
          "x":23,
          "y":750,
          "width":12,
          "height":6,
          "properties":{
            "metrics": [
                [ "AWS/Lambda", "Invocations", "FunctionName", "${var.lv_dashboards_user}-getClientPrescriptions", { "stat": "Sum", "period": 2592000 } ],
                [ ".", "Throttles", ".", ".", { "period": 2592000 } ],
                [ ".", "Duration", ".", ".", { "period": 2592000 } ],
                [ ".", "Errors", ".", ".", { "period": 2592000 } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-getClientPrescriptions",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-P7D",
             "end": "P0D"             
          }          
       },
        {
          "type":"metric",
          "x":23,
          "y":750,
          "width":12,
          "height":6,
          "properties":{
            "metrics": [
                [ "AWS/Lambda", "Invocations", "FunctionName", "${var.lv_dashboards_user}-getCartInstall", { "stat": "Sum", "period": 2592000 } ],
                [ ".", "Throttles", ".", ".", { "period": 2592000 } ],
                [ ".", "Duration", ".", ".", { "period": 2592000 } ],
                [ ".", "Errors", ".", ".", { "period": 2592000 } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-getCartInstall",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-P7D",
             "end": "P0D"             
          }          
       },
        {
          "type":"metric",
          "x":23,
          "y":750,
          "width":12,
          "height":6,
          "properties":{
            "metrics": [
                [ "AWS/Lambda", "Invocations", "FunctionName", "${var.lv_dashboards_user}-getBrandProducts", { "stat": "Sum", "period": 2592000 } ],
                [ ".", "Throttles", ".", ".", { "period": 2592000 } ],
                [ ".", "Duration", ".", ".", { "period": 2592000 } ],
                [ ".", "Errors", ".", ".", { "period": 2592000 } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-getBrandProducts",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-P7D",
             "end": "P0D"             
          }          
       },
           {
          "type":"metric",
          "x":23,
          "y":750,
          "width":12,
          "height":6,
          "properties":{
            "metrics": [
                [ "AWS/Lambda", "Invocations", "FunctionName", "${var.lv_dashboards_user}-getQuestions", { "stat": "Sum", "period": 2592000 } ],
                [ ".", "Throttles", ".", ".", { "period": 2592000 } ],
                [ ".", "Duration", ".", ".", { "period": 2592000 } ],
                [ ".", "Errors", ".", ".", { "period": 2592000 } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-getQuestions",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-P7D",
             "end": "P0D"             
          }          
       }
       
       
       
   ]
 }
 EOF
}


resource "aws_cloudwatch_dashboard" "Cutitronics-API-Gateway" {
  dashboard_name = "${var.lv_dashboards_user}-API-Gateway"

dashboard_body = <<EOF
 {
   "widgets": [
   {
          "type":"metric",
          "x":0,
          "y":0,
          "width":23,
          "height":6,
          "properties":{
            "metrics": [
                  [ "AWS/ApiGateway", "Count", "ApiName", "${var.lv_dashboards_user}-API", "Resource", "/getDefaultPrescriptions", "Stage", "Development", "Method", "POST", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "Latency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "4XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "IntegrationLatency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "5XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-getDefaultPrescriptions",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-PT3H",
             "end": "P0D"           
          }          
       }, 

       {
          "type":"metric",
          "x":0,
          "y":250,
          "width":23,
          "height":6,
          "properties":{
            "metrics": [
                  [ "AWS/ApiGateway", "Count", "ApiName", "${var.lv_dashboards_user}-API", "Resource", "/getUsage", "Stage", "Development", "Method", "POST", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "Latency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "4XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "IntegrationLatency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "5XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-getUsage",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-PT3H",
             "end": "P0D"           
          }          
       },


       {
          "type":"metric",
          "x":0,
          "y":500,
          "width":23,
          "height":6,
          "properties":{
            "metrics": [
                  [ "AWS/ApiGateway", "Count", "ApiName", "${var.lv_dashboards_user}-API", "Resource", "/createSession", "Stage", "Development", "Method", "POST", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "Latency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "4XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "IntegrationLatency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "5XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-createSession",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-PT3H",
             "end": "P0D"           
          }          
       },


       {
          "type":"metric",
          "x":0,
          "y":750,
          "width":23,
          "height":6,
          "properties":{
            "metrics": [
                  [ "AWS/ApiGateway", "Count", "ApiName", "${var.lv_dashboards_user}-API", "Resource", "/getSession", "Stage", "Development", "Method", "POST", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "Latency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "4XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "IntegrationLatency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "5XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-getSession",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-PT3H",
             "end": "P0D"           
          }          
       },


       {
          "type":"metric",
          "x":0,
          "y":1000,
          "width":23,
          "height":6,
          "properties":{
            "metrics": [
                  [ "AWS/ApiGateway", "Count", "ApiName", "${var.lv_dashboards_user}-API", "Resource", "/deleteClient", "Stage", "Development", "Method", "POST", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "Latency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "4XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "IntegrationLatency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "5XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-deleteClient",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-PT3H",
             "end": "P0D"           
          }          
       },

        {
          "type":"metric",
          "x":0,
          "y":1250,
          "width":23,
          "height":6,
          "properties":{
            "metrics": [
                  [ "AWS/ApiGateway", "Count", "ApiName", "${var.lv_dashboards_user}-API", "Resource", "/createClient", "Stage", "Development", "Method", "POST", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "Latency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "4XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "IntegrationLatency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "5XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-createClient",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-PT3H",
             "end": "P0D"           
          }          
       },


       {
          "type":"metric",
          "x":0,
          "y":1500,
          "width":23,
          "height":6,
          "properties":{
            "metrics": [
                  [ "AWS/ApiGateway", "Count", "ApiName", "${var.lv_dashboards_user}-API", "Resource", "/postAnswers", "Stage", "Development", "Method", "POST", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "Latency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "4XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "IntegrationLatency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "5XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-postAnswers",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-PT3H",
             "end": "P0D"           
          }          
       },


       {
          "type":"metric",
          "x":0,
          "y":1750,
          "width":23,
          "height":6,
          "properties":{
            "metrics": [
                  [ "AWS/ApiGateway", "Count", "ApiName", "${var.lv_dashboards_user}-API", "Resource", "/postUpdateSession", "Stage", "Development", "Method", "POST", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "Latency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "4XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "IntegrationLatency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "5XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-postUpdateSession",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-PT3H",
             "end": "P0D"           
          }          
       },


       {
          "type":"metric",
          "x":0,
          "y":2000,
          "width":23,
          "height":6,
          "properties":{
            "metrics": [
                  [ "AWS/ApiGateway", "Count", "ApiName", "${var.lv_dashboards_user}-API", "Resource", "/postGuestNotes", "Stage", "Development", "Method", "POST", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "Latency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "4XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "IntegrationLatency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "5XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-postGuestNotes",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-PT3H",
             "end": "P0D"           
          }          
       },


       {
          "type":"metric",
          "x":0,
          "y":2000,
          "width":23,
          "height":6,
          "properties":{
            "metrics": [
                  [ "AWS/ApiGateway", "Count", "ApiName", "${var.lv_dashboards_user}-API", "Resource", "/postGetGuest", "Stage", "Development", "Method", "POST", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "Latency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "4XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "IntegrationLatency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "5XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-postGetGuest",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-PT3H",
             "end": "P0D"           
          }          
       },

           {
          "type":"metric",
          "x":0,
          "y":2000,
          "width":23,
          "height":6,
          "properties":{
            "metrics": [
                  [ "AWS/ApiGateway", "Count", "ApiName", "${var.lv_dashboards_user}-API", "Resource", "/getClient", "Stage", "Development", "Method", "POST", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "Latency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "4XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "IntegrationLatency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "5XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-getClient",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-PT3H",
             "end": "P0D"           
          }          
       },

           {
          "type":"metric",
          "x":0,
          "y":2000,
          "width":23,
          "height":6,
          "properties":{
            "metrics": [
                  [ "AWS/ApiGateway", "Count", "ApiName", "${var.lv_dashboards_user}-API", "Resource", "/getAllTherapists", "Stage", "Development", "Method", "POST", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "Latency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "4XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "IntegrationLatency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "5XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-getAllTherapists",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-PT3H",
             "end": "P0D"           
          }          
       },

       {
          "type":"metric",
          "x":0,
          "y":2000,
          "width":23,
          "height":6,
          "properties":{
            "metrics": [
                  [ "AWS/ApiGateway", "Count", "ApiName", "${var.lv_dashboards_user}-API", "Resource", "/postLogCartInstall", "Stage", "Development", "Method", "POST", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "Latency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "4XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "IntegrationLatency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "5XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-postLogCartInstall",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-PT3H",
             "end": "P0D"           
          }          
       },

          {
          "type":"metric",
          "x":0,
          "y":2000,
          "width":23,
          "height":6,
          "properties":{
            "metrics": [
                  [ "AWS/ApiGateway", "Count", "ApiName", "${var.lv_dashboards_user}-API", "Resource", "/postUploadUsage", "Stage", "Development", "Method", "POST", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "Latency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "4XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "IntegrationLatency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "5XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-postUploadUsage",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-PT3H",
             "end": "P0D"           
          }          
       },

              {
          "type":"metric",
          "x":0,
          "y":2000,
          "width":23,
          "height":6,
          "properties":{
            "metrics": [
                  [ "AWS/ApiGateway", "Count", "ApiName", "${var.lv_dashboards_user}-API", "Resource", "/getAllClients", "Stage", "Development", "Method", "POST", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "Latency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "4XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "IntegrationLatency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "5XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-getAllClients",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-PT3H",
             "end": "P0D"           
          }          
       },
             {
          "type":"metric",
          "x":0,
          "y":2000,
          "width":23,
          "height":6,
          "properties":{
            "metrics": [
                  [ "AWS/ApiGateway", "Count", "ApiName", "${var.lv_dashboards_user}-API", "Resource", "/postGetAllGuests", "Stage", "Development", "Method", "POST", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "Latency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "4XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "IntegrationLatency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "5XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-postGetAllGuests",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-PT3H",
             "end": "P0D"           
          }          
       },

         {
          "type":"metric",
          "x":0,
          "y":2000,
          "width":23,
          "height":6,
          "properties":{
            "metrics": [
                  [ "AWS/ApiGateway", "Count", "ApiName", "${var.lv_dashboards_user}-API", "Resource", "/getClientPrescriptions", "Stage", "Development", "Method", "POST", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "Latency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "4XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "IntegrationLatency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "5XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-getClientPrescriptions",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-PT3H",
             "end": "P0D"           
          }          
       },

        {
          "type":"metric",
          "x":0,
          "y":2000,
          "width":23,
          "height":6,
          "properties":{
            "metrics": [
                  [ "AWS/ApiGateway", "Count", "ApiName", "${var.lv_dashboards_user}-API", "Resource", "/postTreatmentTypes", "Stage", "Development", "Method", "POST", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "Latency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "4XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "IntegrationLatency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "5XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-postTreatmentTypes",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-PT3H",
             "end": "P0D"           
          }          
       },
         {
          "type":"metric",
          "x":0,
          "y":2000,
          "width":23,
          "height":6,
          "properties":{
            "metrics": [
                  [ "AWS/ApiGateway", "Count", "ApiName", "${var.lv_dashboards_user}-API", "Resource", "/getCartInstall", "Stage", "Development", "Method", "POST", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "Latency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "4XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "IntegrationLatency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "5XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-getCartInstall",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-PT3H",
             "end": "P0D"           
          }          
       },
         {
          "type":"metric",
          "x":0,
          "y":2000,
          "width":23,
          "height":6,
          "properties":{
            "metrics": [
                  [ "AWS/ApiGateway", "Count", "ApiName", "${var.lv_dashboards_user}-API", "Resource", "/getBrandProducts", "Stage", "Development", "Method", "POST", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "Latency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "4XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "IntegrationLatency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "5XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-getBrandProducts",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-PT3H",
             "end": "P0D"           
          }          
       },
       
       {
          "type":"metric",
          "x":0,
          "y":2000,
          "width":23,
          "height":6,
          "properties":{
            "metrics": [
                  [ "AWS/ApiGateway", "Count", "ApiName", "${var.lv_dashboards_user}-API", "Resource", "/getQuestions", "Stage", "Development", "Method", "POST", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "Latency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "4XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ],
                  [ ".", "IntegrationLatency", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Average" } ],
                  [ ".", "5XXError", ".", ".", ".", ".", ".", ".", ".", ".", { "period": 2592000, "stat": "Sum" } ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-getQuestions",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-PT3H",
             "end": "P0D"           
          }          
       }
   ]
 }
 EOF
}


# RDS

resource "aws_cloudwatch_dashboard" "Cutitronics-RDS" {

   dashboard_name = "${var.lv_dashboards_user}-RDS"
   
   
dashboard_body = <<EOF
 {
   "widgets": [
       {
          "type":"metric",
          "x":0,
          "y":250,
          "width":24,
          "height":6,
          "properties":{
            "metrics": [
        [ "AWS/RDS", "CPUUtilization", "DBInstanceIdentifier", "${var.lv_database_name}" ]
            ],
            "region": "eu-west-2",
             "view": "timeSeries",
             "title": "${var.lv_dashboards_user}-CPU Usage",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-PT3H",
             "end": "P0D"             
          }
       },
       {
          "type":"metric",
          "x":0,
          "y":0,
          "width":12,
          "height":3,
          "properties":{
            "metrics": [
               [ "AWS/RDS", "FreeStorageSpace", "DBInstanceIdentifier", "${var.lv_database_name}" ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-Available RDS Storage",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-PT3H",
             "end": "P0D"             
          }
       }, 
       {
          "type":"metric",
          "x":12,
          "y":0,
          "width":12,
          "height":3,
          "properties":{
            "metrics": [
               [ "AWS/RDS", "DatabaseConnections", "DBInstanceIdentifier", "${var.lv_database_name}" ]
            ],
            "region": "eu-west-2",
             "view": "singleValue",
             "title": "${var.lv_dashboards_user}-No RDS Connections",
             "period": 300,
             "width": 1840,
             "height": 250,
             "start": "-PT3H",
             "end": "P0D"             
          }
       }             
   
   
   ]
 }
 EOF
}       
