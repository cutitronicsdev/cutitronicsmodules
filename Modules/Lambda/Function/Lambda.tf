
resource "aws_iam_role" "iam_for_lambda" {
  name = "${var.v_environment}-${var.lv_function_name}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
 EOF
} 

resource "aws_lambda_function" "createLambda" {

  filename            = var.lv_filename
  function_name       = var.lv_function_name
  handler             = var.lv_handler
  description         = var.lv_description
  runtime             = var.lv_runtime
  memory_size         = var.lv_memory_size
  timeout             = var.lv_timeout
  layers              = [var.lv_layers]
  role                = "${aws_iam_role.iam_for_lambda.arn}"

  environment {
      variables = {
        v_username    = var.lv_rds_username,
        v_password    = var.lv_rds_password,
        v_db_host     = var.lv_rds_host,
        v_database    = var.lv_rds_name
      }

  }
}

# CloudWatch

resource "aws_cloudwatch_log_group" "cloudwatch" {
  name              =  "/aws/lambda/${var.lv_function_name}"
  retention_in_days = 14
}


# See also the following AWS managed policy: AWSLambdaBasicExecutionRole
resource "aws_iam_policy" "lambda_logging" {

  name        = "${var.v_environment}-${var.lv_function_name}"
  path        = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "lambda_logs" {
  role        = "${aws_iam_role.iam_for_lambda.name}"
  policy_arn  = "${aws_iam_policy.lambda_logging.arn}"
}