variable "lv_filename" {
    type = string
    description = "(Optional) The path to the function's deployment package within the local filesystem"
}


variable "lv_function_name" {
    type = string
    description = ""
}


variable "lv_handler" {
    type = string
    description = ""
}


variable "lv_description" {
    type = string
    description = ""
}


variable "lv_runtime" {
    type = string
    description = ""
}

variable "lv_memory_size" {
    type = number
    description = ""
}


variable "lv_timeout" {
    type = number
    description = ""
}


variable "lv_layers" {
    type = string
    description = ""
}


# RDS


variable "lv_rds_username" {
    type = string
    description = ""
}


variable "lv_rds_password" {
    type = string
    description = ""
}


variable "lv_rds_host" {
    type = string
    description = ""
}


variable "lv_rds_name" {
    type = string
    description = ""
}




variable "v_environment" {
    type = string
    description = "(optional) describe your variable"
}