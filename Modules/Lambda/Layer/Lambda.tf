resource "aws_lambda_layer_version" "pymysql" {
  filename   = var.lv_filename
  layer_name = var.lv_layer_name
}