# RDS Driver Layer


output "aws_lambda_layer_version_pymysql_layer_arn" {
    value = aws_lambda_layer_version.pymysql.layer_arn
}

# created_date - The date this resource was created.

output "aws_lambda_layer_version_pymysql_created_date" {
    value = aws_lambda_layer_version.pymysql.created_date
}

# source_code_size - The size in bytes of the function .zip file.

output "aws_lambda_layer_version_pymysql_source_code_size" {
    value = aws_lambda_layer_version.pymysql.source_code_size
}

# version - This Lamba Layer version. 

output "aws_lambda_layer_version_pymysql_version" {
    value = aws_lambda_layer_version.pymysql.version
}
