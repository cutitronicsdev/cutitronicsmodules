variable "lv_filename" {
    type = string
    description = "(Optional) The path to the function's deployment package within the local filesystem. If defined, The s3_-prefixed options cannot be used."
}


variable "lv_layer_name" {
    type = string
    description = "(Required) A unique name for your Lambda Layer"
}