# Create Mariadb instance
resource "aws_default_vpc" "default" {}

resource "aws_db_instance" "MariaDB" {
	engine                    = var.v_rds_engine
	allocated_storage         = var.v_rds_allocated_storage
	instance_class            = var.v_rds_instance_class
	name                      = var.v_rds_name
	username                  = var.v_rds_username
	password                  = var.v_rds_password
	publicly_accessible       = true
  	skip_final_snapshot  	  = true
	vpc_security_group_ids    = ["${aws_security_group.mariadb-db-sg.id}"]
	identifier = "cutitronics-${var.v_rds_identifier}"

  	tags = {
      	Name        = var.v_rds_name
    	Environment = var.v_environment
  	}	

  	enabled_cloudwatch_logs_exports = ["audit", "general"]
  	maintenance_window         		= "sun:02:00-sun:04:00"
}

resource "aws_security_group" "mariadb-db-sg" {
  name   = "${var.v_environment}-mariadb-security-group"
  vpc_id = "${aws_default_vpc.default.id}"

  ingress {
    protocol    = "tcp"
    from_port   = 3306
    to_port     = 3306
    cidr_blocks = ["0.0.0.0/0"]
  }

	ingress {
		protocol    = "tcp"
		from_port   = 3306
		to_port     = 3306
		cidr_blocks = ["62.232.218.2/32"]
	}  

  egress {
    protocol    = -1
    from_port   = 0 
    to_port     = 0 
    cidr_blocks = ["0.0.0.0/0"]
  }
}