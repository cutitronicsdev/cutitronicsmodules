# address - The hostname of the RDS instance. See also endpoint and port.

output "cutitronics_MariaDB_address"{
    value = aws_db_instance.MariaDB.address
}

# arn - The ARN of the RDS instance.

output "cutitronics_MariaDB_arn"{
    value = aws_db_instance.MariaDB.arn
}

# allocated_storage - The amount of allocated storage.

output "cutitronics_MariaDB_allocated_storage"{
    value = aws_db_instance.MariaDB.allocated_storage
}

# availability_zone - The availability zone of the instance.

output "cutitronics_MariaDB_availability_zone"{
    value = aws_db_instance.MariaDB.availability_zone
}

# backup_retention_period - The backup retention period.

output "cutitronics_MariaDB_backup_retention_period"{
    value = aws_db_instance.MariaDB.backup_retention_period
}

# backup_window - The backup window.

output "cutitronics_MariaDB_backup_window"{
    value = aws_db_instance.MariaDB.backup_window
}

# ca_cert_identifier - Specifies the identifier of the CA certificate for the DB instance.

output "cutitronics_MariaDB_ca_cert_identifier"{
    value = aws_db_instance.MariaDB.ca_cert_identifier
}

# domain - The ID of the Directory Service Active Directory domain the instance is joined to

output "cutitronics_MariaDB_domain"{
    value = aws_db_instance.MariaDB.domain
}

# domain_iam_role_name - The name of the IAM role to be used when making API calls to the Directory Service.

output "cutitronics_MariaDB_domain_iam_role_name"{
    value = aws_db_instance.MariaDB.domain_iam_role_name
}

# endpoint - The connection endpoint in address:port format.

output "cutitronics_MariaDB_endpoint"{
    value = aws_db_instance.MariaDB.endpoint
}

# engine - The database engine.

output "cutitronics_MariaDB_engine"{
    value = aws_db_instance.MariaDB.engine
}

# hosted_zone_id - The canonical hosted zone ID of the DB instance (to be used in a Route 53 Alias record).

output "cutitronics_MariaDB_hosted_zone_id"{
    value = aws_db_instance.MariaDB.hosted_zone_id
}

# id - The RDS instance ID.

output "cutitronics_MariaDB_id"{
    value = aws_db_instance.MariaDB.id
}

# instance_class- The RDS instance class.

output "cutitronics_MariaDB_instance_class"{
    value = aws_db_instance.MariaDB.instance_class
}

# maintenance_window - The instance maintenance window.

output "cutitronics_MariaDB_maintenance_window"{
    value = aws_db_instance.MariaDB.maintenance_window
}

# multi_az - If the RDS instance is multi AZ enabled.

output "cutitronics_MariaDB_multi_az"{
    value = aws_db_instance.MariaDB.multi_az
}

# name - The database name.

output "cutitronics_MariaDB_name"{
    value = aws_db_instance.MariaDB.name
}

# port - The database port.

output "cutitronics_MariaDB_port"{
    value = aws_db_instance.MariaDB.port
}

# resource_id - The RDS Resource ID of this instance.

output "cutitronics_MariaDB_resource_id"{
    value = aws_db_instance.MariaDB.resource_id
}

# status - The RDS instance status.

output "cutitronics_MariaDB_status"{
    value = aws_db_instance.MariaDB.status
}

# storage_encrypted - Specifies whether the DB instance is encrypted.

output "cutitronics_MariaDB_storage_encrypted"{
    value = aws_db_instance.MariaDB.storage_encrypted
}

# username - The master username for the database.

output "cutitronics_MariaDB_username"{
    value = aws_db_instance.MariaDB.username
}