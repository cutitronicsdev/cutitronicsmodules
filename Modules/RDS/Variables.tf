
variable "v_environment" {}
variable "v_client_name" {}

variable "v_rds_username" {
  description = "(Required) - Username for the master DB user"
  type = string
}

variable "v_rds_password" {
  description = "(Required) - Username for the master DB user"
  type = string
}

variable "v_rds_name" {
  description = "(Optional) The name of the database to create when the DB instance is created"
  type = string
}


variable "v_rds_allocated_storage" {
  description = "(Required unless a snapshot_identifier or replicate_source_db is provided) The allocated storage in gibibytes. If max_allocated_storage is configured, this argument represents the initial storage allocation and differences from the configuration will be ignored automatically when Storage Autoscaling occurs"
  type = number
}


variable "v_rds_storage_type" {
  description = "(Optional) One of standard (magnetic), gp2 (general purpose SSD), or io1 (provisioned IOPS SSD). The default is io1 if iops is specified, gp2 if not"
  type = string
}


variable "v_rds_engine"{
  description = "Required unless a snapshot_identifier or replicate_source_db is provided"
  type = string
}


variable "v_rds_engine_version"{
  description = "(Optional) The engine version to use."
  type = string
}


variable "v_rds_instance_class"{
  description = "(Required) The instance type of the RDS instance."
  type = string
}


variable "v_rds_parameter_group_name"{
  description = "(Optional) Name of the DB parameter group to associate"
  type = string
}


variable "v_rds_identifier"{
  type = string
}