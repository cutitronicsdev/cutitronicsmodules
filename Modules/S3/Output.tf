# S3 bucket output

# id - The name of the bucket. 

output "cutitronics_s3_id" {
  value = aws_s3_bucket.report_store.id
}

# arn - The ARN of the bucket. Will be of format arn:aws:s3:::bucketname.

output "cutitronics_s3_arn"{
    value = aws_s3_bucket.report_store.arn
}

# bucket_domain_name - The bucket domain name. Will be of format bucketname.s3.amazonaws.com.

output "cutitronics_s3_bucket_domain_name"{
    value = aws_s3_bucket.report_store.bucket_domain_name
}

# bucket_regional_domain_name - The bucket region-specific domain name

output "cutitronics_s3_bucket_regional_domain_name"{
    value = aws_s3_bucket.report_store.bucket_regional_domain_name
}

# hosted_zone_id - The Route 53 Hosted Zone ID for this bucket's region.

output "cutitronics_s3_hosted_zone_id"{
    value = aws_s3_bucket.report_store.hosted_zone_id
}

# region - The AWS region this bucket resides in.

output "cutitronics_s3_region"{
    value = aws_s3_bucket.report_store.region
}

# website_endpoint - The website endpoint, if the bucket is configured with a website. If not, this will be an empty string.

output "cutitronics_s3_website_endpoint"{
    value = aws_s3_bucket.report_store.website_endpoint
}

# website_domain - The domain of the website endpoint, if the bucket is configured with a website. If not, this will be an empty string. This is used to create Route 53 alias records.

output "cutitronics_s3_website_domain"{
    value = aws_s3_bucket.report_store.website_domain
}