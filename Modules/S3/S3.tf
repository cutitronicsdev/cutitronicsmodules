
resource "aws_s3_bucket" "report_store" {
  bucket = "${var.v_environment}-${var.v_s3_bucket_name}"
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "AES256"
      }
    }
  }

  tags = {
    Name = "${var.v_environment}-${var.v_s3_bucket_name}"
    Environment = var.v_environment
  }

}


resource "aws_s3_bucket_public_access_block" "report_store_access" {
  bucket = aws_s3_bucket.report_store.id
  block_public_acls   = true
  block_public_policy = true
}